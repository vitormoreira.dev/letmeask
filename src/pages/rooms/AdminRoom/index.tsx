import { useNavigate, useParams } from "react-router-dom";

import deleteImg from "../../../assets/images/delete.svg";
import checkImg from "../../../assets/images/check.svg";
import answerImg from "../../../assets/images/answer.svg";

import { Button } from "../../../components/Button";
import { Question } from "../../../components/Question";
import { RoomCode } from "../../../components/RoomCode";
// import { useAuth } from '../hooks/useAuth';
import { useRoom } from "../../../hooks/useRoom";
import { database } from "../../../services/firebase";

import "./styles.scss";
import { Header } from "../../../components/Header";
import { Footer } from "../../../components/Footer";

type RoomParams = {
  id: string;
};

export function AdminRoom() {
  // const { user } = useAuth();
  const navigate = useNavigate();
  const params = useParams<RoomParams>();
  const roomId = params.id || "";

  const { title, questions } = useRoom(roomId);

  async function handleEndRoom() {
    await database.ref(`rooms/${roomId}`).update({
      endedAt: new Date(),
    });

    navigate("/");
  }

  async function handleDeleteQuestion(questionId: string) {
    if (window.confirm("Tem certeza que você deseja excluir esta pergunta?")) {
      await database.ref(`rooms/${roomId}/questions/${questionId}`).remove();
    }
  }

  async function handleCheckQuestionAsAnswered(questionId: string) {
    await database.ref(`rooms/${roomId}/questions/${questionId}`).update({
      isAnswered: true,
    });
  }

  async function handleHighlightQuestion(questionId: string) {
    await database.ref(`rooms/${roomId}/questions/${questionId}`).update({
      isHighlighted: true,
    });
  }

  return (
    <div id="page-room">
      <Header>
        <RoomCode code={roomId} />
        <Button isOutlined onClick={handleEndRoom}>
          Encerrar sala
        </Button>
      </Header>

      <main>
        <div className="room-title">
          <h1>
            Nome da sala: <i>{title}</i>
          </h1>
          {questions.length > 0 && <span>{questions.length} pergunta(s)</span>}
        </div>

        <div className="question-list">
          {questions.some((q) => q) ? (
            questions.map((question) => {
              return (
                <Question
                  key={question.id}
                  content={question.content}
                  author={question.author}
                  isAnswered={question.isAnswered}
                  isHighlighted={question.isHighlighted}
                >
                  {!question.isAnswered && (
                    <>
                      <button
                        type="button"
                        onClick={() =>
                          handleCheckQuestionAsAnswered(question.id)
                        }
                      >
                        <img
                          src={checkImg}
                          alt="Marcar pergunta como respondida"
                        />
                      </button>
                      <button
                        type="button"
                        onClick={() => handleHighlightQuestion(question.id)}
                      >
                        <img src={answerImg} alt="Dar destaque à pergunta" />
                      </button>
                    </>
                  )}
                  <button
                    type="button"
                    onClick={() => handleDeleteQuestion(question.id)}
                  >
                    <img src={deleteImg} alt="Remover pergunta" />
                  </button>
                </Question>
              );
            })
          ) : (
            <div className="empty-list">
              <svg viewBox="0 0 24 24">
                <path
                  d="M2.2,16.06L3.88,12L2.2,7.94L6.26,6.26L7.94,2.2L12,3.88L16.06,2.2L17.74,6.26L21.8,7.94L20.12,12L21.8,16.06L17.74,17.74L16.06,21.8L12,20.12L7.94,21.8L6.26,17.74L2.2,16.06M4.81,9L6.05,12L4.81,15L7.79,16.21L9,19.19L12,17.95L15,19.19L16.21,16.21L19.19,15L17.95,12L19.19,9L16.21,7.79L15,4.81L12,6.05L9,4.81L7.79,7.79L4.81,9M11,15H13V17H11V15M11,7H13V13H11V7"
                />
              </svg>
              <h2>Ainda não existe nenhuma pergunta!</h2>
              <p>Divulgue sua sala para receber perguntas.</p>
            </div>
          )}
        </div>
      </main>

      <Footer></Footer>
    </div>
  );
}
