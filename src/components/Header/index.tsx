import { ReactNode } from "react";
import logoImg from "../../assets/images/logo.svg";

import "./styles.scss";

type HeaderProps = {
  children?: ReactNode;
};

export function Header(params: HeaderProps) {
  return (
    <header>
      <div className="content">
        <a href="/">
          <img src={logoImg} alt="Letmeask" />
        </a>
        <div>{params.children}</div>
      </div>
    </header>
  );
}
