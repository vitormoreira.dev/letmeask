import gitlabImg from "../../assets/images/gitlab.svg";
import linkedinImg from '../../assets/images/linkedin.svg';
import gmailImg from '../../assets/images/gmail.svg';

import "./styles.scss";

export function Footer() {
  return (
    <footer id="footer">
      <div className="content">
          <div>
              <p>Criado por: <i>Vitor Moreira</i></p>
          </div>
          <div>
            <a href="https://gitlab.com/vitormoreira.dev/" target="_blank" rel="noopener noreferrer">
              <img src={ gitlabImg } alt="Gitlab" />
            </a>
            <a href="https://www.linkedin.com/in/vitor-moreira-1c/" target="_blank" rel="noopener noreferrer">
              <img src={ linkedinImg } alt="LinkedIn" />
            </a>
            <a href="mailto:vitormoreira.dev@gmail.com" target="_blank" rel="noopener noreferrer">
              <img src={ gmailImg } alt="LinkedIn" />
            </a>
          </div>
      </div>
    </footer>
  );
}
